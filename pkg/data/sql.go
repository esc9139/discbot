package data

import (
	"fmt"
	"os"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var (
	DB_HOST     = os.Getenv("DB_HOST")
	DB_PORT     = getenvInt("DB_PORT")
	DB_USER     = os.Getenv("DB_USER")
	DB_PASSWORD = os.Getenv("DB_PASSWORD")
	DB_NAME     = os.Getenv("DB_NAME")
)

var DB *sqlx.DB

func InitDB() {
	//dbinfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)
	dbinfo := fmt.Sprintf("%s:%s@(%s:%d)/%s", DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)

	datab, err := sqlx.Open("mysql", dbinfo)
	DB = datab
	fmt.Println(err)
}

func getenvInt(key string) int {
	s := os.Getenv(key)
	v, err := strconv.Atoi(s)
	if err != nil {
		return 0
	}
	return v
}
