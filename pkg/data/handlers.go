package data

import (
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func RecMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	msgsplit := strings.Split(m.Content, " ")

	if msgsplit[0] == ".bag" || msgsplit[0] == ".advbag" {
		userid := m.Author.ID
		if len(msgsplit) == 2 {
			uid, err := cleanid(msgsplit[1])
			if err != nil {
				return
			}
			userid = uid
		}
		if msgsplit[0] == ".bag" {
			Bag(userid, s, m, false)
		} else {
			Bag(userid, s, m, true)
		}
	}

	if msgsplit[0] == ".bagmate" {
		userid := m.Author.ID
		if len(msgsplit) == 2 {
			uid, err := cleanid(msgsplit[1])
			if err != nil {
				return
			}
			userid = uid
		}
		Bagmate(userid, s, m)
	}

	if msgsplit[0] == ".ibag" && len(msgsplit) > 1 {
		s.ChannelMessageSend(m.ChannelID, IBag(m.Author.ID, cleanArgList(msgsplit[1:])))
	}

}

func cleanid(userid string) (string, error) {
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		return "", err
	}
	return reg.ReplaceAllString(userid, ""), nil
}

func cleanArgList(args []string) []string {
	joined := strings.Join(args, " ")
	args = strings.Split(joined, ",")
	for i, s := range args {
		args[i] = strings.Trim(s, " ")
	}
	return args
}
