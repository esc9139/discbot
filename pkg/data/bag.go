package data

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/jmoiron/sqlx"
)

func IBag(userid string, discs []string) string {
	var bagdiscs []DBIBagDiscs

	query, args, err := sqlx.In("select name, ID, (select userid from bag where userid = ? and discid=ID) as userid from discs where NAME in (?)", userid, discs)
	if err != nil {
		return "Error finding users currently bagged discs"
	}

	query = DB.Rebind(query)

	err = DB.Select(&bagdiscs, query, args...)
	if err != nil {
		return "Error finding users currently bagged discs"
	}

	var remstr []string
	var addstr []string
	var nofstr []string
	var errstr []string
	var retstr []string

	for _, disc := range bagdiscs {
		for i := len(discs) - 1; i >= 0; i-- {
			if strings.ToLower(discs[i]) == strings.ToLower(disc.Name) {
				discs = append(discs[:i], discs[i+1:]...)
			}
		}
		if disc.User.Valid {
			_, err := DB.Exec("DELETE FROM bag WHERE userid = ? and discid = ?", userid, disc.ID)
			if err != nil {
				errstr = append(errstr, "`"+strings.Title(disc.Name)+"`")
			} else {
				remstr = append(remstr, "`"+strings.Title(disc.Name)+"`")
			}
		} else {
			_, err := DB.Exec("INSERT INTO bag (userid, discid) VALUES (?,?)", userid, disc.ID)
			if err != nil {
				errstr = append(errstr, "`"+strings.Title(disc.Name)+"`")
			} else {
				addstr = append(addstr, "`"+strings.Title(disc.Name)+"`")
			}
		}
	}
	for _, disc := range discs {
		nofstr = append(nofstr, "`"+strings.Title(disc)+"`")
	}

	if len(addstr) > 0 {
		retstr = append(retstr, "Disc "+strings.Join(addstr, ",")+" has been added to your bag.")
	}
	if len(remstr) > 0 {
		retstr = append(retstr, "Disc "+strings.Join(remstr, ",")+" has been removed from your bag.")
	}
	if len(nofstr) > 0 {
		retstr = append(retstr, "Disc "+strings.Join(nofstr, ",")+" was not found.")
	}
	if len(errstr) > 0 {
		retstr = append(retstr, "There was an error adding or removing disc "+strings.Join(errstr, ",")+".")
	}

	return strings.Join(retstr, "\n")
}

func Bag(userid string, s *discordgo.Session, m *discordgo.MessageCreate, adv bool) {
	var bagdiscs []DBBagDiscs
	var baginfo DBBagInfo
	var maninfo []DBManInfo

	user, err := s.User(userid)
	if err != nil {
		fmt.Println(err)
		s.ChannelMessageSend(m.ChannelID, "Error getting userinfo.")
		return
	}

	err = DB.Select(&bagdiscs, `select b.NAME, b.SPEED, b.GLIDE, b.TURN, b.FADE, b.ismid, b.isputter from bag a, discs b where a.discid  = b.ID and a.userid = ? ORDER BY b.FADE DESC`, userid)
	if err != nil {
		fmt.Println(err)
		s.ChannelMessageSend(m.ChannelID, "Error getting bag discs for this user.")
		return
	}
	if adv {
		err = DB.Select(&maninfo, `select b.MANUFACTURER, COUNT(b.ID) as COUNT from bag a, discs b where a.discid  = b.ID and a.userid = ? GROUP BY b.MANUFACTURER ORDER BY COUNT(b.ID) DESC`, userid)
		if err != nil {
			fmt.Println(err)
			s.ChannelMessageSend(m.ChannelID, "Error getting disc manufacturer info for this user.")
			return
		}
	}
	_ = DB.Get(&baginfo, `select bagname, bagphoto, COALESCE((select name from discs where id=putter),'') as putter from bag_options where userid = ? LIMIT 1`, userid)
	// if err != nil {
	// 	fmt.Println(err)
	// 	s.ChannelMessageSend(m.ChannelID, "Error getting bag info for this user.")
	// 	return
	// }

	var putters []string
	var mids []string
	var fairways []string
	var control []string
	var distance []string

	for _, disc := range bagdiscs {
		if disc.Speed < 4 || disc.IsPutter == 1 {
			if adv {
				putters = append(putters, fmt.Sprintf("%s: %.2g, %.2g, %.2g, %.2g", strings.Title(disc.Name), disc.Speed, disc.Glide, disc.Turn, disc.Fade))
			} else {
				putters = append(putters, strings.Title(disc.Name))
			}
		} else if (disc.Speed > 3 && disc.Speed < 6) || disc.IsMid == 1 {
			if adv {
				mids = append(mids, fmt.Sprintf("%s: %.2g, %.2g, %.2g, %.2g", strings.Title(disc.Name), disc.Speed, disc.Glide, disc.Turn, disc.Fade))
			} else {
				mids = append(mids, strings.Title(disc.Name))
			}
		} else if disc.Speed > 5 && disc.Speed < 9 {
			if adv {
				fairways = append(fairways, fmt.Sprintf("%s: %.2g, %.2g, %.2g, %.2g", strings.Title(disc.Name), disc.Speed, disc.Glide, disc.Turn, disc.Fade))
			} else {
				fairways = append(fairways, strings.Title(disc.Name))
			}
		} else if disc.Speed > 8 && disc.Speed < 11 {
			if adv {
				control = append(control, fmt.Sprintf("%s: %.2g, %.2g, %.2g, %.2g", strings.Title(disc.Name), disc.Speed, disc.Glide, disc.Turn, disc.Fade))
			} else {
				control = append(control, strings.Title(disc.Name))
			}
		} else if disc.Speed > 10 {
			if adv {
				distance = append(distance, fmt.Sprintf("%s: %.2g, %.2g, %.2g, %.2g", strings.Title(disc.Name), disc.Speed, disc.Glide, disc.Turn, disc.Fade))
			} else {
				distance = append(distance, strings.Title(disc.Name))
			}
		}
	}

	if baginfo.Name == "" {
		baginfo.Name = "Set a bag nickname using .nick command"
	}

	joinchar := ", "
	if adv {
		joinchar = "\n"
	}

	embed := &discordgo.MessageEmbed{
		Color:       0x0099ff,
		Title:       baginfo.Name,
		Description: user.Username + "'s Discs are:",
		Fields:      []*discordgo.MessageEmbedField{},
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf(`%s bags %d molds`, user.Username, len(bagdiscs)),
		},
	}

	if baginfo.Putter != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  "Putting With:",
			Value: strings.Title(baginfo.Putter),
		})
	}

	if len(putters) > 0 {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Putter",
			Value:  strings.Join(putters, joinchar),
			Inline: adv,
		})
	}

	if len(mids) > 0 {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Midrange",
			Value:  strings.Join(mids, joinchar),
			Inline: adv,
		})
	}

	if len(fairways) > 0 {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Fairway Drivers",
			Value:  strings.Join(fairways, joinchar),
			Inline: adv,
		})
	}

	if len(control) > 0 {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Control Drivers",
			Value:  strings.Join(control, joinchar),
			Inline: adv,
		})
	}

	if len(distance) > 0 {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Distance Drivers",
			Value:  strings.Join(distance, joinchar),
			Inline: adv,
		})
	}

	if adv {
		var manu []string
		for _, m := range maninfo {
			manu = append(manu, fmt.Sprintf("%s (%d)", strings.Title(m.Manufacturer), m.Count))
		}
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  user.Username + "'s bag count by brand:",
			Value: strings.Join(manu, "\n"),
		})
	}

	if baginfo.Photo != "" {
		embed.Thumbnail = &discordgo.MessageEmbedThumbnail{
			URL: baginfo.Photo,
		}
	}

	s.ChannelMessageSendEmbed(m.ChannelID, embed)
}

func Bagmate(userid string, s *discordgo.Session, m *discordgo.MessageCreate) {
	user, err := s.User(userid)
	if err != nil {
		fmt.Println(err)
		s.ChannelMessageSend(m.ChannelID, "Error getting userinfo.")
		return
	}

	var mates []DBBagMate

	err = DB.Select(&mates, `select distinct b.userid, GROUP_CONCAT(c.name) as discs, count(b.discid) as matecount from (select discid from bag where userid = ?) a, bag b, discs c where a.discid = b.discid and b.discid = c.id and b.userid != ? group by b.userid order by matecount desc`, userid, userid)
	if err != nil {
		fmt.Println(err)
		s.ChannelMessageSend(m.ChannelID, "Error getting bagmate for this user.")
		return
	}

	if len(mates) < 1 {
		s.ChannelMessageSend(m.ChannelID, "Error getting bagmate for this user.")
		return
	}

	maxcount := mates[0].Count
	var mateusers []string
	var matediscs []string

	for _, mate := range mates {
		if mate.Count == maxcount {
			mu, err := s.User(mate.User)
			if err != nil {
				s.ChannelMessageSend(m.ChannelID, "Error getting userinfo.")
				return
			}

			mateusers = append(mateusers, mu.Username)
			discs := strings.Split(mate.Discs, ",")
			for i, d := range discs {
				discs[i] = strings.Title(d)
			}
			matediscs = append(matediscs, strings.Join(discs, ","))
		} else {
			break
		}
	}

	mids := "bagmate is"
	if len(mateusers) > 1 {
		mids = "bagmates are"
		mateusers[len(mateusers)-1] = "and " + mateusers[len(mateusers)-1]
	}

	embed := &discordgo.MessageEmbed{
		Color:  0xff007f,
		Title:  user.Username + "'s " + mids + ": " + strings.Join(mateusers, ", ") + "; how cute!",
		Fields: []*discordgo.MessageEmbedField{},
	}

	for i, _ := range mateusers {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  fmt.Sprintf("%s and %s carry %d of the same discs:", user.Username, mateusers[i], maxcount),
			Value: matediscs[i],
		})
	}

	s.ChannelMessageSendEmbed(m.ChannelID, embed)
}
