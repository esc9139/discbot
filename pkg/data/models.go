package data

import "database/sql"

type DBBagDiscs struct {
	Name     string  `db:"NAME"`
	Speed    float64 `db:"SPEED"`
	Glide    float64 `db:"GLIDE"`
	Turn     float64 `db:"TURN"`
	Fade     float64 `db:"FADE"`
	IsMid    int     `db:"ismid"`
	IsPutter int     `db:"isputter"`
}

type DBBagInfo struct {
	Name   string `db:"bagname"`
	Photo  string `db:"bagphoto"`
	Putter string `db:"putter"`
}

type DBManInfo struct {
	Manufacturer string `db:"MANUFACTURER"`
	Count        int    `db:"COUNT"`
}

type DBIBagDiscs struct {
	Name string         `db:"name"`
	ID   int            `db:"ID"`
	User sql.NullString `db:"userid"`
}

type DBBagMate struct {
	User  string `db:"userid"`
	Discs string `db:"discs"`
	Count int    `db:"matecount"`
}
